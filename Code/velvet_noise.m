function [] = velvet_noise(d,L,fs)
    %*****inputs******
    % d: the impulse density
    % L: the Length of the samplesize
    % fs: the sample rate
    %*****Outputs******
    % s_d: Impulse train

    T_d = round(fs/d); %Spacing Average
    M = round(L/T_d);
    t = (1:T_d:L);
    
    t = t+round((rand(1,length(t))*(T_d - 1)));
    s_d = zeros(1,L);
    s_d(t) = 2*round(rand(1,length(t))) - 1;

    stem(s_d)
    
end