function [s_d] = velvet_noise2(d,L,fs)
    %*****inputs******
    % d: the impulse density
    % L: the Length of the samplesize
    % fs: the sample rate
    %*****Outputs******
    % s_d: Impulse train

    T_d = round(fs/d);                             %Spacing Average

    s_d = zeros(1,L);                                %Impulse train array
    M = round(L/T_d);                             %Amount of impulses
    m = 0 : M-1;                    

    k_d = min(round(m*T_d + rand(1,M)*(T_d-1)),2000); %Determines the positions of the 
                                                                                             % impulses in s_d and limits 
                                                                                             % to 2000 samples as per size of array
    s_d(k_d) = 2*round(rand(1,M)) - 1;                                %sets impulse valus at k_d arrays positions

    stem(s_d,"filled")

end