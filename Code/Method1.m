
%Find out if application of an amplification enveloppe derived from the
%same sound can be used to model the audio natural fluctuation in order to
%have it sound less static.

[y,fs] = audioread("../audio/Airplane_1s_original_mono.wav");
y =y(:,1);
original_sound = audioplayer(y,fs);
T =1/fs;
noise = randn(1,fs);

impulse =[1; zeros(fs-1,1)];
x_imp = 0:T:1-T;

LP = [100 1000 10000];

y = y';
x = 0:T:(length(y)-1)*T;

% figure(1)
% plot(x,y)
% xlabel("Sec (s)")
% ylabel("Amplitude")

z = fft(y);
% figure(2)
% mag_plot(z,fs)

%***************************White Noise Method***************************
%LP 100
    a1 = lpc(y, LP(1));
    h_1 = imp_analysis(impulse,a1);
    y_white_noise_1 = LP_analysis(noise,a1);
    white_noise_1 = audioplayer(y_white_noise_1,fs);

%LP 1000
    a2 = lpc(y, LP(2));

    h_2 = imp_analysis(impulse,a2);
    y_white_noise_2 = LP_analysis(noise,a2);
    white_noise_2 = audioplayer(y_white_noise_2,fs);

%LP 10 000
    %tic
    %for i = 1:100
    a3 = lpc(y, LP(3));
    h_3 = imp_analysis(impulse,a3);
    
    y_white_noise_3 = LP_analysis(noise,a3);
    %end
    %time(1) = toc/100;

    white_noise_3 = audioplayer(y_white_noise_3,fs);
    audiowrite("Piano_LP.wav",y_white_noise_3,fs)

%***************************Sound Test 1***************************
    %playblocking(original_sound)
%     pause(1)
%     playblocking(white_noise_1)
%     pause(1)
%     playblocking(white_noise_2)
    %pause(1)
    %playblocking(white_noise_3)

%***************************Velvet Noise Method*************************** 
s_d = velvet_noise2(2024,200,fs);
% tic
% for i = 1:100
y_velvet = conv(s_d,y);
y_velvet = y_velvet/max(abs(y_velvet));
% end
% time(2) = toc/100;
x_velvet = 0:T:(length(y_velvet)-1)*T;

velve_noise = audioplayer(y_velvet,fs);
audiowrite("Piano_velvet.wav",y_velvet,fs)

%***************************Sound Test 2***************************
%pause(1)
%playblocking(velve_noise)

%***************************IFFT Method*************************** 
% tic
y_ifft = [];
for i = 1:10
y_ifft = [y_ifft ifft_generator(z)];
y_ifft = y_ifft/max(abs(y_ifft));
end
% time(3) = toc/100;

ifft_noise = audioplayer(y_ifft,fs);
audiowrite("Piano_ifft.wav",y_ifft,fs)

%pause(1)
%playblocking(ifft_noise)

%***************************Plots*************************** 
figure(1)

tiledlayout(1,4)

nexttile
plot(x,y)
title("Original Signal")
xlabel("Seconds (sec)")
ylabel("Amplitude")

nexttile
plot(y_white_noise_3)
title("10 000^{th} order LP Method")
xlabel("Seconds (sec)")
%ylabel("Amplitude")

nexttile
plot(x_velvet,y_velvet)
xlim([0,x_velvet(length(x_velvet))])
title("Velvet Noise Method")
xlabel("Seconds (sec)")
%ylabel("Amplitude")

nexttile
plot(y_ifft)
title("IFFT Method")
xlabel("Seconds (sec)")
%ylabel("Amplitude")


figure(2)

mag_plot(z,fs)
hold on
mag_plot(fft(y_white_noise_3),fs)
hold off

figure(3)

plot(h_3)
title("Impulse Response of the 10 000^{th} LP filter")
xlabel("Seconds")
ylabel("Amplitude")



%***************************Functions*************************** 

function [y_ifft] = ifft_generator(z)
    z = z;
    L = length(z);
    rnd_theta= -pi + (2*pi).*rand(1,L/2-1); 

    z(2:L/2)=z(2:L/2).*exp(1i*rnd_theta);
    z(L/2+2:L)= z(L/2+2:L).*exp(-1i*flip(rnd_theta,2));
    
    y_ifft = real(ifft(z));
end


function [] = mag_plot(z, fs)

    n = length(z);
    f = (0:n-1)*(fs/n);
    mag = 20*log10(abs(z));
    
    semilogx(f(1:floor(n/2+1)), mag(1:floor(n/2+1))); % Takes only half of the array at f_max/2
    xlim([0,f(length(f))])
    title("Magnitude Frequency Response")
    xlabel("Frequency (Hz)")
    ylabel("Magnitude (dB)")

end

function [] = phase_plot(z, fs)

    n = length(z);
    f = (0:n-1)*(fs/n);
    phase = angle(z);
    
    plot(f(1:floor(n/2+1)), phase(1:floor(n/2+1))); % Takes only half of the array at f_max/2
    title("Phase Response")
    xlabel("Frequency (Hz)")
    ylabel("Angle (deg)")

end

function [f_noise] = LP_analysis(noise, a)
    f_noise = filter(1, a, noise);
    f_noise = f_noise/max(abs(f_noise));
end

function [h] = imp_analysis(imp,a)
    h = filter(1,a,imp);
end