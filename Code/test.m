[y,fs] = audioread("../audio/Airplane_1s_original_mono.wav");
[L, N] = size(y);
Y = fft(y);

rnd_theta= -pi + (2*pi).*rand(L/2-1,1); 
Y(2:L/2)=Y(2:L/2).*exp(1i*rnd_theta);
Y(L/2+2:L)=Y(L/2+2:L).*exp(-1i*flip(rnd_theta,2));

randy = real(ifft(Y));

sound(randy,fs);
